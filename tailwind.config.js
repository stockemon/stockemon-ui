module.exports = {
  darkMode: 'class', // 'media' or 'class'
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        // 'gunmetal': '#2e3440',
        // 'charcoal': '#3b4252',
        // 'light-charcoal': '#434c5e',
        // 'lighter-charcoal': '#4c566a',
        'rich-black-fogra-39': '#0a0a0a',
        'eerie-black': '#141414',
        'light-eerie-black': '#1f1f1f',
        'jet': '#292929',
        'light-jet': '#333333',
        'onyx': '#3d3d3d',
        'davys-grey': '#474747',
        'light-davys-grey': '#525252',
      },
      height: {
        'root-main-content': 'calc(100% - 112px)',
        'root-sidebar-main': 'calc(100% - 56px)',
      },
    },
  },
  plugins: [],
}

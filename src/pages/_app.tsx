import React from "react";
import { Provider } from "react-redux";
import type { AppProps } from "next/app";

import '../globals.css';
import { store } from "../redux/store";

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </>
  );
}

// App.getInitialProps = async ({ ctx }: any) => {
//   if (typeof ctx.req.cookies.token !== 'undefined' && !ctx.req.cookies.token.length) {
//     if (!['/login', '/signup'].includes(ctx.pathname)) {
//       ctx.res.writeHead(307, { Location: '/login' });
//       ctx.res.end();
//     }

//     return { props: {} };
//   } else {
//     if (!['/login', '/signup'].includes(ctx.pathname)) {
//       try {
//         const isTokenValid = await axios.post(
//           'http://localhost:3000/api/v1/auth/verify',
//           { token: ctx.req.cookies.token },
//         );

//         console.log(isTokenValid);
  
//         return { props: {} };
//       } catch (error: any) {
//         ctx.res.writeHead(307, { Location: '/login' });
//         ctx.res.end();
//       }
//     } else {
//       return { props: {} };
//     }
//   }
// }

export default App;

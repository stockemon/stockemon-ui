import React from 'react';
import { GetServerSidePropsContext } from 'next';
import { TrendingUpIcon } from '@heroicons/react/outline';

import Root from '../components/layout/Root';
import { useGetPortfoliosByUserQuery } from '../redux/reducers/api';
import { Bar, BarChart, Cell, LabelList, Pie, PieChart, ReferenceLine, ResponsiveContainer, XAxis, YAxis } from 'recharts';

const fakeBarData = [
  { name: 'MSFT', change: 37.3 },
  { name: 'AAPL', change: 50.8 },
  { name: 'HD', change: -2.4 },
  { name: 'ASML', change: -9.8 },
  { name: 'TSM', change: -44.5 },
  { name: 'GOOG', change: 100.2 },
  { name: 'TSLA', change: 88.8 },
  { name: 'ZIM', change: -20.01 },
  { name: 'Z', change: -10.2 },
  { name: 'U', change: 10.1 },
  { name: 'SQ', change: 23.4 },
  { name: 'CRWD', change: 9.8 },
].sort((a, b) => a.change < b.change ? 1 : -1).slice(0, 10);

const fakePieData1 = [
  { name: 'MSFT', proportion: 10 },
  { name: 'ASML', proportion: 12 },
  { name: 'TSM', proportion: 3.75 },
  { name: 'AAPL', proportion: 10 },
  { name: 'Z', proportion: 8 },
  { name: 'Cash', proportion: 56.25 },
].sort((a, b) => a.proportion > b.proportion ? 1 : -1);

const fakePieData2 = [
  { name: 'Cash', proportion: 56.25 },
  { name: 'Stock', proportion: 43.75 },
].sort((a, b) => a.proportion > b.proportion ? 1 : -1);

const fakeTable = [
  { name: 'MSFT', proportion: 10, shares: 8, cost: 500 },
  { name: 'AAPL', proportion: 12, shares: 12, cost: 500 },
  { name: 'TSM', proportion: 3.75, shares: 10, cost: 500 },
  { name: 'ASML', proportion: 10, shares: 1, cost: 500 },
  { name: 'Z', proportion: 8, shares: 5, cost: 500 },
  { name: 'MSFT', proportion: 10, shares: 8, cost: 500 },
  { name: 'AAPL', proportion: 12, shares: 12, cost: 500 },
  { name: 'TSM', proportion: 3.75, shares: 10, cost: 500 },
  { name: 'ASML', proportion: 10, shares: 1, cost: 500 },
  { name: 'Z', proportion: 8, shares: 5, cost: 500 },
  { name: 'MSFT', proportion: 10, shares: 8, cost: 500 },
  { name: 'AAPL', proportion: 12, shares: 12, cost: 500 },
  { name: 'TSM', proportion: 3.75, shares: 10, cost: 500 },
  { name: 'ASML', proportion: 10, shares: 1, cost: 500 },
  { name: 'Z', proportion: 8, shares: 5, cost: 500 },
].sort((a, b) => a.proportion < b.proportion ? 1 : -1);

const renderCustomizedLabel = ({
  cx, cy, midAngle, innerRadius, outerRadius, percent, index,
}: any) => {
  console.log(cx, cy);
  const RADIAN = Math.PI / 180;
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="#374151" textAnchor="middle" dominantBaseline="central">
      {fakePieData1[index].name}
    </text>
  );
}

const renderInnerCustomizedLabel = ({
  cx, cy, midAngle, innerRadius, outerRadius, percent, index,
}: any) => {
  console.log(cx, cy);
  const RADIAN = Math.PI / 180;
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="#374151" textAnchor="middle" dominantBaseline="central">
      {fakePieData2[index].name}
    </text>
  );
}

const pieSectionColors = ['#9f8fb0', '#cbc0d3', '#efd3d7', '#feeafa', '#dee2ff'];

const IndexPage = () => {
  const {
    data: portfolios = Array(5).fill({
      name: 'Boss Inventory',
      currency: 'USD',
      totalAsset: '25271.53',
      unrealized: '487.56',
    }), isLoading, isSuccess, isError, error,
  } = useGetPortfoliosByUserQuery();

  return (
    <Root portfolios={portfolios}>
      <div className="w-full max-w-screen-md mx-auto ">
        <h2 className="text-xl text-gray-300">Summary</h2>
        <div className="w-full flex flex-row my-8">
          <div className="w-1/2 flex flex-col rounded-md bg-light-eerie-black">
            <h4 className="text-base text-gray-400 pt-2 px-4">Total Asset</h4>
            <div className="flex flex-row w-full justify-center items-center px-4 pt-2 pb-4">
              <h5 className="text-2xl font-bold text-gray-200 mr-8">
                25271.63
              </h5>
              <div className="flex flex-row items-center text-gray-400">
                <TrendingUpIcon className="w-5 h-5 text-green-400" />
                <p className="text-gray-300 ml-2">251.52</p>
                <p className="text-green-600 ml-2">+7%</p>
              </div>
            </div>
            <div className="w-full flex flex-row border border-x-0 border-b-0 border-t-gray-600">
              <div className="flex flex-col px-4 py-2 w-1/2 border border-y-0 border-l-0 border-r-gray-600">
                <h5 className="text-base text-gray-400">Cash</h5>
                <h6 className="text-lg text-gray-500 mx-auto">18000.72</h6>
              </div>
              <div className="flex flex-col px-4 py-2 w-1/2">
                <h5 className="text-base text-gray-400">Stock</h5>
                <h6 className="text-lg text-gray-500 mx-auto">9000.72</h6>
              </div>
            </div>
          </div>
          <div className="w-1/2 flex flex-col rounded-md bg-light-eerie-black ml-4">
            <div className="flex flex-row py-2 px-8 justify-between border border-x-0 border-t-0 border-b-gray-600">
              <h5 className="text-gray-400">
                Unrealized
              </h5>
              <h6 className="text-gray-300">
                501.02
              </h6>
            </div>
            <div className="flex flex-row py-2 px-8 justify-between border border-x-0 border-t-0 border-b-gray-600">
              <h5 className="text-gray-400">
                Realized
              </h5>
              <h6 className="text-gray-300">
                1501.02
              </h6>
            </div>
            <div className="flex flex-row py-2 px-8 justify-between border border-x-0 border-t-0 border-b-gray-600">
              <h5 className="text-gray-400">
                Dividend
              </h5>
              <h6 className="text-gray-300">
                103.3
              </h6>
            </div>
            <div className="flex flex-row py-2 px-8 justify-between">
              <h5 className="text-gray-400">
                Intra-Day
              </h5>
              <h6 className="text-gray-300">
                36.87
              </h6>
            </div>
          </div>
        </div>
        <div className="w-full h-8 flex flex-row">
          <div style={{ width: `${9000/16000*100}%` }} className="h-full flex flex-row items-center justify-center bg-gray-300 rounded-tl-md rounded-bl-md">
            <h6 className="text-gray-600">{`Cash (${9000 / 16000 * 100}%)`}</h6>
          </div>
          <div style={{ width: `${7000/16000*100}%` }} className="h-full flex flex-row items-center justify-center bg-gray-500 rounded-tr-md rounded-br-md">
            <h6 className="text-gray-200">{`Stock (${7000/16000 * 100}%)`}</h6>
          </div>
        </div>
        <h3 className="text-base text-gray-400 mt-8">
          Top 10 Performance
        </h3>
        <ResponsiveContainer height={400} className="mt-2 mb-8">
          <BarChart data={fakeBarData}>
            <XAxis dataKey="name" stroke="#e5e7eb" interval={0} />
            <Bar dataKey="change" fill="#000">
              <LabelList dataKey="change" fill="#e5e7eb"/>
            </Bar>
          </BarChart>
        </ResponsiveContainer>
        <h3 className="text-base text-gray-400 mt-8">
          Asset Distribution
        </h3>
        <div className="flex flex-row mt-8 mb-8 justify-between h-96">
          <div className="w-1/2">
            <ResponsiveContainer height="100%">
              <PieChart>
                <Pie
                  data={fakePieData1}
                  dataKey="proportion"
                  innerRadius={120}
                  outerRadius={180}
                  label={renderCustomizedLabel}
                  labelLine={false}
                >
                  {
                    fakePieData1.map((entry, index) => (
                      <Cell key={`outer-pie-${index}`} fill={pieSectionColors[index % 5]} />
                    ))
                  }
                </Pie>
                <Pie
                  data={fakePieData2}
                  dataKey="proportion"
                  innerRadius={70}
                  outerRadius={110}
                  label={renderInnerCustomizedLabel}
                  labelLine={false}
                >
                  {
                    fakePieData2.map((entry, index) => (
                      <Cell key={`inner-pie-${index}`} fill={pieSectionColors[index % 5]} />
                    ))
                  }
                </Pie>
              </PieChart>
            </ResponsiveContainer>
          </div>
          <div className="w-1/2 flex flex-col ml-8">
            <div className="relative shadow overflow-x-auto border-b border-eerie-black sm:rounded-lg hidden-scrollbar">
              <table className="w-full divide-y divide-eerie-black">
                <thead className="sticky top-0 bg-eerie-black">
                  <tr>
                    <th className="pl-6 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Name
                    </th>
                    <th className="py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Proportion
                    </th>
                    <th className="py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Shares
                    </th>
                    <th className="pr-6 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Cost
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-light-davys-grey divide-y divide-eerie-black">
                  {
                    fakeTable.map((record) => (
                      <tr key={record.name}>
                        <td className="pl-6 py-1 whitespace-nowrap text-sm font-medium text-gray-200">{record.name}</td>
                        <td className="py-1 whitespace-nowrap text-sm text-gray-400">{`${record.proportion}%`}</td>
                        <td className="py-1 whitespace-nowrap text-sm text-gray-400">{record.shares}</td>
                        <td className="pr-6 py-1 whitespace-nowrap text-sm text-gray-400">{record.cost}</td>
                      </tr>
                    ))
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </Root>
  );
}

export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
  console.log(ctx.req.cookies);
  console.log(ctx.req.url);

  return { props: {} };
};

export default IndexPage;

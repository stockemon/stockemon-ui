import React from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { GetServerSidePropsContext } from 'next';

const LoginPage = () => {
  const router = useRouter();
  const [loginProcessing, setLoginProcessing] = React.useState(false);
  const [loginErrorStatus, setLoginErrorStatus] = React.useState('');
  const usernameRef: React.RefObject<HTMLInputElement> = React.createRef();
  const passwordRef: React.RefObject<HTMLInputElement> = React.createRef();

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>): Promise<any> => {
    e.preventDefault();
    setLoginProcessing(true);

    const username = usernameRef.current?.value;
    const password = passwordRef.current?.value;

    if (!username) {
      setLoginErrorStatus('Username cannot be empty');
      setLoginProcessing(false);
      return undefined;
    }

    if (!password) {
      setLoginErrorStatus('Password cannot be empty');
      setLoginProcessing(false);
      return undefined;
    }

    try {
      await axios.post(
        'http://localhost:3000/api/v1/auth/login',
        { username, password },
        { withCredentials: true },
      );
      setLoginProcessing(false);
      return router.push('/');
    } catch (error: any) {
      console.log(error);
      setLoginErrorStatus(error.response.data);
      setLoginProcessing(false);
      return undefined;
    }
  };

  return (
    <div className="h-screen flex items-center justify-center bg-gray-400 py-12 px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          {/* <img className="mx-auto h-20 w-auto" src="/pocket-manager.svg" alt="Workflow" /> */}
          <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-800">
            Pocket Manager
          </h2>
        </div>
        <form className="mt-8 space-y-6" onSubmit={handleSubmit}>
          <input type="hidden" name="remember" value="true" />
          <div className="rounded-md shadow-sm -space-y-px">
            <div>
              <label className="sr-only">Username</label>
              <input
                ref={usernameRef}
                type="text"
                placeholder="Username"
                className="appearance-none rounded-none relative block w-full px-3 py-3 border placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none sm:text-md"
              />
            </div>
            <div>
              <label className="sr-only">Password</label>
              <input
                ref={passwordRef}
                type="password"
                placeholder="Password"
                className="appearance-none rounded-none relative block w-full px-3 py-3 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none sm:text-md"
              />
            </div>
          </div>
          <div>
            <button type="submit" className="group relative w-full flex items-center justify-center py-2 px-4 border border-transparent text-md font-bold rounded-md text-gray-300 bg-gray-600 focus:outline-none focus:bg-gray-700">
              <svg className={`animate-spin -ml-1 mr-3 h-5 w-5 text-gray-300 ${loginProcessing ? 'animate-spin' : 'hidden'}`} fill="none" viewBox="0 0 24 24">
                <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"></circle>
                <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
              </svg>
              Sign in
            </button>
            {
              loginErrorStatus && (
                <p className="font-bold text-sm text-red-700 mt-2">
                  { loginErrorStatus }
                </p>
              )
            }
          </div>
          <div className="flex items-center justify-between space-y-4 sm:space-y-0 flex-col sm:flex-row">
            <a href="#" className="font-bold text-sm text-gray-700">
              Need an account ?
            </a>
            <a href="#" className="font-bold text-sm text-gray-700">
              Forgot your password ?
            </a>
          </div>
        </form>
      </div>
    </div>
  );
}

export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
  console.log(ctx.req.cookies);
  console.log(ctx.req.url);

  return { props: {} };
};

export default LoginPage;

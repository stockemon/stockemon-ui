import { configureStore } from '@reduxjs/toolkit';

import { apiSlice } from './reducers/api';

export const store = configureStore({
  reducer: {
    // themeMode : themeModeReducer,
    [apiSlice.reducerPath]: apiSlice.reducer,
  },
  middleware: defaultMiddleware => defaultMiddleware().concat(apiSlice.middleware),
});

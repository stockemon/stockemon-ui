import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    token: typeof window !== 'undefined' ? localStorage.getItem('token') : '',
    authenticated: false,
  },
  reducers: {
    // changeDarkMode: (state, action) => {
    //   state. = action.payload;
    // },
  },
});

// Action creators are generated for each case reducer function
// export const { changeDarkMode } = themeMode.actions;

// export default themeMode.reducer;

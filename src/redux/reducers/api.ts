import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const apiSlice = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({
    baseUrl: 'http://localhost:3000/api/v1',
    credentials: 'include',
  }),
  endpoints: (builder) => ({
    // https://github.com/reduxjs/redux-toolkit/issues/1676
    getPortfoliosByUser: builder.query<any, void>({
      query: () => '/portfolios',
    }),
  }),
});

export const {
  useGetPortfoliosByUserQuery,
} = apiSlice;

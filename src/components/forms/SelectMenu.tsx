import React from 'react';
import classnames from 'classnames';
import { ChevronDownIcon, ChevronUpIcon, CheckIcon } from '@heroicons/react/solid';

interface ISelectMenuProps {
  className: string,
  value: string,
  setValue: React.Dispatch<React.SetStateAction<string>>,
  options: string[],
}

const SelectMenu = ({ className, value, setValue, options }: ISelectMenuProps) => {
  const [open, setOpen] = React.useState(false);

  const rootClassname = classnames('relative', 'h-9', className);
  const buttonClassname = classnames(
    'px-3',
    'bg-jet',
    'shadow-sm',
    'rounded-md',
    'sm:text-sm',
    'w-full h-9',
    'border border-charcoal',
    'flex flex-row items-center justify-between',
    'focus:outline-none focus:border-indigo-600',
  );
  const optionsListClassname = classnames(
    'z-10',
    'bg-jet',
    'absolute',
    'bottom-10',
    'rounded-md',
    'sm:text-sm',
    'overflow-auto',
    'w-full max-h-56',
    'focus:outline-none',
    'select-menu-scrollbar',
    'border border-gray-500',
  );
  const optionClassname = classnames(
    'py-2 px-3',
    'select-none',
    'text-gray-300',
    'cursor-pointer',
    'flex flex-row justify-between items-center',
  );

  return (
    <div className={rootClassname}>
      <button type="button" className={buttonClassname} onClick={() => setOpen(!open)}>
        <p className="text-gray-200 truncate font-normal">{value}</p>
        { open && <ChevronUpIcon className="h-5 w-5 text-gray-300" /> }
        { !open && <ChevronDownIcon className="h-5 w-5 text-gray-300" /> }
      </button>
      {
        open && (
          <ul role="listbox" className={optionsListClassname}>
            {
              options.map((option, index) => (
                <li
                  role="option"
                  aria-selected={option === value}
                  key={`select-menu-option-${index}`}
                  className={optionClassname}
                  onClick={() => { setValue(option); setOpen(!open); }}
                >
                  <p className="truncate">{option}</p>
                  { option === value && <CheckIcon className="h-5 w-5 flex items-center" /> }
                </li>
              ))
            }
          </ul>
        )
      }
    </div>
  );
}

export default SelectMenu;

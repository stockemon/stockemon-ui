import React from 'react';
import { LibraryIcon, DocumentReportIcon } from '@heroicons/react/outline';

import SelectMenu from '../forms/SelectMenu';

const Toolbox = () => {
  const [currency, setCurrency] = React.useState("USD");
  const [tooltipsController, setTooltipsController] = React.useState(Array(4).fill(false));

  const updateTooltipsController = (index: number) => {
    const newTooltipsController = tooltipsController;
    newTooltipsController[index] = !newTooltipsController[index];
    setTooltipsController([...newTooltipsController]);
  }

  return (
    <div className="w-full h-14 absolute bottom-0 bg-light-davys-grey">
      <div className="w-full h-full px-4 lg:px-0 lg:max-w-screen-md xl:max-w-screen-lg mx-auto flex flex-row items-center">
        <span className="inline-flex items-center mr-4 px-3 py-1 rounded-md text-sm font-medium bg-gray-200 text-jet">
          Toolbox
        </span>
        {
          Array(4).fill(0).map((_, toolIndex) => (
            <div
              key={`tool-option-${toolIndex}`} className={`${toolIndex !== 3 ? 'mr-2' : ''} relative cursor-pointer`}
              onMouseEnter={(e) => updateTooltipsController(toolIndex)}
              onMouseLeave={(e) => updateTooltipsController(toolIndex)}
            >
              {
                tooltipsController[toolIndex] && (
                  <>
                    <p className="absolute bottom-0 -translate-x-1/2 left-2 text-sm bg-black text-gray-200 px-4 py-2 whitespace-nowrap mb-9 rounded-md">
                      { toolIndex === 0 ? 'Cash Deposit' : toolIndex === 1 ? 'Cash Withdrawal' : toolIndex === 2 ? 'Buy Stock' : 'Sell Stock' }
                    </p>
                    <span className="absolute bottom-0 w-0 h-0 mb-7 left-1 border border-l-8 border-l-transparent border-r-8 border-r-transparent border-t-8 border-t-black border-b-transparent" />
                  </>
                )
              }
              {
                toolIndex === 0 && <LibraryIcon
                  className="w-6 h-6 text-green-500 pointer-events-none"
                />
              }
              {
                toolIndex === 1 && <LibraryIcon
                  className="w-6 h-6 text-red-500 pointer-events-none"
                />
              }
              {
                toolIndex === 2 && <DocumentReportIcon
                  className="w-6 h-6 text-green-500 pointer-events-none"
                />
              }
              {
                toolIndex === 3 && <DocumentReportIcon
                  className="w-6 h-6 text-red-500 pointer-events-none"
                />
              }
            </div>
          ))
        }
        <SelectMenu
          className="w-48"
          value={currency}
          setValue={setCurrency}
          options={["USD", "HKD", "JPY", "CNY", 'ABC', "BCD", "EFG"]}
        />
      </div>
    </div>
  );
}

export default Toolbox;

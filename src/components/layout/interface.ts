export interface ISidebarPortfolio {
  name: string,
  currency: string,
  totalAsset: string,
  unrealized: string,
}

export interface ISidebarProps {
  className?: string,
  displayName: string,
  portfolios: ISidebarPortfolio[],
}

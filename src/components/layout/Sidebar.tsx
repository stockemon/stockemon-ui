/* eslint-disable @next/next/no-img-element */
import React from 'react';
import classnames from 'classnames';
import {
  CogIcon, PlusIcon, TrendingUpIcon, TrendingDownIcon,
} from '@heroicons/react/outline';

import { ISidebarProps } from './interface';

const Sidebar = ({ className, displayName, portfolios }: ISidebarProps) => {
  const rootClassname = classnames('relative', 'w-96 h-full', 'bg-light-eerie-black', className);
  const portfolioSectionClassname = classnames(
    'flex flex-col',
    'overflow-y-auto',
    'transparent-scrollbar',
    'w-full h-root-sidebar-main',
  );
  const portfolioClassname = (index: number) => classnames(
    'w-full',
    'rounded-md',
    'px-4 py-2 my-1',
    'cursor-pointer',
    'flex flex-row items-center',
    {
      'bg-light-jet': index === 0,
      'hover:bg-jet': index !== 0,
    },
  );
  const userSectionClassname = classnames(
    'px-4',
    'absolute',
    'bottom-0',
    'w-full h-14',
    'bg-rich-black-fogra-39',
    'flex flex-row justify-between items-center',
  );

  return (
    <div className={rootClassname}>
      <div className={portfolioSectionClassname}>
        <img src="/logo.svg" alt="Stockemon" className="w-full px-10 py-4" />
        <div className="w-full h-full flex flex-col px-6 py-4">
          <div className="w-full flex flex-row items-center justify-between">
            <h2 className="text-gray-400 text-lg">
              Portfolios
            </h2>
            <PlusIcon className="w-5 h-5 text-gray-200 cursor-pointer" />
          </div>
          <div className="w-full flex flex-col py-3">
            {
              portfolios.map((portfolio, index) => (
                <div
                  key={`sidebar-portfolio-${index}`}
                  className={portfolioClassname(index)}
                >
                  { !portfolio.unrealized.includes('-') && <TrendingUpIcon className="w-6 h-6 mr-4 text-green-400" /> }
                  { portfolio.unrealized.includes('-') && <TrendingDownIcon className="w-6 h-6 mr-4 text-red-400" /> }
                  <div className="w-full flex flex-col">
                    <div className="flex flex-row justify-between items-end">
                      <h3 className="text-sm text-gray-300">
                        { portfolio.name }
                      </h3>
                      <h5 className="text-xs text-gray-500">
                        { portfolio.currency }
                      </h5>
                    </div>
                    <div className="flex flex-row">
                      <p className="text-sm text-gray-400">
                        {`${portfolio.totalAsset} | ${portfolio.unrealized}`}
                      </p>
                    </div>
                  </div>
                </div>
              ))
            }
          </div>
        </div>
      </div>
      <div className={userSectionClassname}>
        <h4 className="text-gray-200 truncate">
          { displayName }
        </h4>
        <CogIcon className="ml-4 w-8 h-8 text-gray-200" />
      </div>
    </div>
  );
}

export default Sidebar;

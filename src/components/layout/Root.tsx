import React from 'react';

import Sidebar from './Sidebar';
import Toolbox from './Toolbox';
import { ISidebarPortfolio } from './interface';

const Root = ({
  children,
  portfolios,
}: {
  children?: any,
  portfolios: ISidebarPortfolio[],
}) => {
  return (
    <div className="flex flex-row w-full h-full bg-light-jet">
      <Sidebar displayName="Chrissssssssssssssssssss" portfolios={portfolios} />
      <div className="relative flex flex-col w-full h-full">
        <div className="w-full h-14 flex flex-row justify-between items-center bg-blue-500">
          <p>Portfolio name</p>
          <div className="flex flex-row">
            <p>Icon 1</p>
            <p>Icon 2</p>
          </div>
        </div>
        <div className='w-full h-root-main-content py-8 overflow-auto transparent-scrollbar'>
          { children }
        </div>
        <Toolbox />
      </div>
    </div>
  );
}

export default Root;
